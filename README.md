# Client for the TOPdesk REST APIs

# Testing

Run unit tests with

```bash
python -m pytest .
```

To include integration tests you will have to create a config.yaml file with content

```yaml
url: "http://url/to/actual/endpoint/"
headers: { "X-Gravitee-Api-Key": "your-api-key"}
```

and then run pytest with

```bash
python -m pytest -m "integration" .
```
