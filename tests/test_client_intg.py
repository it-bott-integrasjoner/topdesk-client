import pytest

from topdesk_client import get_client
from topdesk_client.models import IncidentCreateBody, Incident, CallerCreate


@pytest.mark.integration
def test_post_incident(integration_test_config):
    client = get_client(integration_test_config)
    icb = IncidentCreateBody(request="foo", caller=CallerCreate(dynamicName="bar"))
    r = client.post_incident(icb)
    assert isinstance(r, Incident)
