import types
import json

from topdesk_client.models import Operator, Person, Incident, IncidentCreateBody


def test_get_operator_by_login_name(client,
                                    base_url,
                                    requests_mock,
                                    single_operator_list_data):
    identity = 'fj'
    requests_mock.get((client.urls.get_operators() +
                       '?topdesk_login_name=' + identity),
                      json=single_operator_list_data)
    expected = client.get_operator(identity)
    assert isinstance(expected, Operator)


def test_get_operator(client,
                      base_url,
                      requests_mock,
                      operator,
                      operator_data):
    requests_mock.get(client.urls.get_operator(operator.id),
                      json=operator_data)
    expected = client.get_operator(operator)
    assert isinstance(expected, Operator)


def test_list_operators(client, base_url, requests_mock, operator_list_data):
    requests_mock.get(client.urls.get_operators(),
                      json=operator_list_data)
    expected = client.list_operators()
    assert isinstance(expected, types.GeneratorType)
    for x in expected:
        assert isinstance(x, Operator)


def _patch_operator_for_comparison(obj, operator):
    sent_operator = json.loads(obj)
    del sent_operator['password']
    try:
        sent_operator['id'] = operator.inclusive_dict()['id']
    except KeyError:
        pass
    sent_operator['branch'] = operator.branch.inclusive_dict()
    return Operator.from_dict(sent_operator)


def test_create_operator(client,
                         base_url,
                         requests_mock,
                         uncreated_operator,
                         operator_as_dict):
    m = requests_mock.post(client.urls.get_operators(),
                           status_code=201,
                           json=operator_as_dict)
    client.create_operator(uncreated_operator)

    object_sent = m.request_history[-1].text

    assert(_patch_operator_for_comparison(
        object_sent, uncreated_operator) == uncreated_operator)


def test_update_operator(client,
                         base_url,
                         requests_mock,
                         operator_as_dict,
                         operator):
    m = requests_mock.put(client.urls.get_operator(operator.id),
                          status_code=200,
                          json=operator_as_dict)
    client.update_operator(operator)

    object_sent = m.request_history[-1].text

    assert _patch_operator_for_comparison(object_sent, operator) == operator

def test_link_operator_to_operator_filter(client,
                                          requests_mock,
                                          operator):
    m = requests_mock.post(client.urls.get_operator_filter(operator.id),
                           status_code=204)
    client.link_operator_to_operator_filter(operator, '9001')

    object_sent = m.request_history[-1].json()
    assert object_sent == [{'id': '9001'}]

def test_get_person_by_login_name(client,
                                  base_url,
                                  requests_mock,
                                  single_person_list_data):
    identity = 'fj'
    requests_mock.get((client.urls.get_persons() +
                       '?ssp_login_name=' + identity),
                      complete_qs=True,
                      json=single_person_list_data)
    expected = client.get_person(identity)
    assert isinstance(expected, Person)


def test_get_person(client, base_url, requests_mock, person, person_data):
    requests_mock.get(client.urls.get_person(person.id),
                      json=person_data)
    expected = client.get_person(person)
    assert isinstance(expected, Person)


def test_list_persons(client, base_url, requests_mock, person_list_data):
    requests_mock.get(client.urls.get_persons(),
                      json=person_list_data)
    expected = client.list_persons()
    assert isinstance(expected, types.GeneratorType)
    for x in expected:
        assert isinstance(x, Person)


def _patch_person_for_comparison(obj, person):
    sent_person = json.loads(obj)
    del sent_person['password']
    try:
        sent_person['id'] = person.inclusive_dict()['id']
    except KeyError:
        pass
    sent_person['branch'] = person.branch.inclusive_dict()
    return Person.from_dict(sent_person)


def test_create_person(client,
                       base_url,
                       requests_mock,
                       uncreated_person):
    m = requests_mock.post(client.urls.get_persons())
    client.create_person(uncreated_person)

    object_sent = m.request_history[-1].text

    assert(_patch_person_for_comparison(object_sent,
                                        uncreated_person) == uncreated_person)


def test_update_person(client,
                       base_url,
                       requests_mock,
                       person):
    m = requests_mock.patch(client.urls.get_person(person.id))
    client.update_person(person)

    object_sent = m.request_history[-1].text

    assert(_patch_person_for_comparison(object_sent, person) == person)


def test_post_incident(
    client,
    requests_mock,
    incident_response,
    incident_create_body
):
    """
    Check that the client method behaves as expected

    Assumes that the response from the real api behaves like our
    fixture

    Both fixtures are examples from
    https://developers.topdesk.com/explorer/?page=incident#/incident/post_incidents
    """
    requests_mock.post(
        client.urls.get_incidents(),
        json=incident_response,
        status_code=201
    )
    expected_response = Incident(**incident_response)
    actual_response = client.post_incident(
        IncidentCreateBody(**incident_create_body)
    )

    assert isinstance(actual_response, Incident)
    assert actual_response == expected_response
