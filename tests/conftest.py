import pytest
import os
import json

import yaml

from topdesk_client import get_client
from topdesk_client.models import Operator, Person


@pytest.fixture
def base_url():
    return 'https://localhost'


@pytest.fixture
def config(base_url):
    return {
        'url': base_url,
        'username': 'username',
        'password': 'i LiEk tH0 W3aR h4T5!'
    }


@pytest.fixture
def integration_test_config():
    with open("config.yaml") as f:
        config_file = f.read()
    return yaml.load(config_file, Loader=yaml.FullLoader)


@pytest.fixture
def client(config):
    return get_client(config)


def load_json_file(name):
    here = os.path.realpath(os.path.join(os.getcwd(),
                                         os.path.dirname(__file__)))
    with open(os.path.join(here, 'fixtures', name)) as f:
        data = json.load(f)
    return data


@pytest.fixture
def operator_data():
    return load_json_file('operator.json')


@pytest.fixture
def single_operator_list_data():
    return load_json_file('single_operator_list.json')


@pytest.fixture
def operator():
    d = load_json_file('operator.json')
    return Operator.from_dict(d)

@pytest.fixture
def operator_as_dict():
    return load_json_file('operator.json')

@pytest.fixture
def uncreated_operator():
    return Operator.from_dict(
        load_json_file('uncreated_operator.json'))


@pytest.fixture
def operator_list_data():
    return load_json_file('operator_list.json')


@pytest.fixture
def person_data():
    return load_json_file('person.json')


@pytest.fixture
def single_person_list_data():
    return load_json_file('single_person_list.json')


@pytest.fixture
def person():
    d = load_json_file('person.json')
    return Person.from_dict(d)


@pytest.fixture
def uncreated_person():
    return Person.from_dict(
        load_json_file('uncreated_person.json'))


@pytest.fixture
def person_list_data():
    return load_json_file('person_list.json')


@pytest.fixture
def incident_create_body():
    return load_json_file("incident_create_body.json")


@pytest.fixture
def incident_response():
    return load_json_file("incident_response.json")


def pytest_collection_modifyitems(config, items):
    """Modify collection to skip tests marked with integration."""
    if config.option.keyword or config.option.markexpr:
        return
    skip_integration = pytest.mark.skip(
        reason='Not running with pytest -m "integration"'
    )
    for item in items:
        if "integration" in item.keywords:
            item.add_marker(skip_integration)

