#!/usr/bin/env python3
import sys
import setuptools
import setuptools.command.test


def get_requirements(filename):
    """ Read requirements from file. """
    with open(filename, mode='rt', encoding='utf-8') as f:
        for line in f:
            # TODO: Will not work with #egg-info
            requirement = line.partition('#')[0].strip()
            if not requirement:
                continue
            yield requirement


def get_textfile(filename):
    """ Get contents from a text file. """
    with open(filename, mode='rt', encoding='utf-8') as f:
        return f.read().lstrip()


def get_packages():
    """ List of (sub)packages to install. """
    return setuptools.find_packages('.', include=('topdesk_client',
                                                  'topdesk_client.*'))


class PyTest(setuptools.command.test.test):
    """ Run tests using pytest.

    From `http://doc.pytest.org/en/latest/goodpractices.html`.

    """

    user_options = [('pytest-args=', 'a', "Arguments to pass to pytest")]

    def initialize_options(self):
        super().initialize_options()
        self.pytest_args = []

    def run_tests(self):
        import shlex
        import pytest
        args = self.pytest_args
        if args:
            args = shlex.split(args)
        errno = pytest.main(args)
        raise SystemExit(errno)


def run_setup():
    setup_requirements = ['setuptools_scm']
    test_requirements = list(get_requirements('requirements-test.txt'))
    install_requirements = list(get_requirements('requirements.txt'))

    setuptools.setup(
        name='topdesk-client',
        description='Client for the TOPdesk Supporting Files REST API',
        long_description=get_textfile('README.md'),
        long_description_content_type='text/markdown',

        url='https://bitbucket.usit.uio.no/projects/INT/repos/topdesk-client',
        author='USIT, University of Oslo',
        author_email='bnt-int@usit.uio.no',

        use_scm_version=True,
        packages=get_packages(),
        python_requires='>=3',
        setup_requires=setup_requirements,
        install_requires=install_requirements,
        tests_require=test_requirements,
        cmdclass={
            'test': PyTest,
        },
        classifiers=[
            'Development Status :: 3 - Alpha',
            'Intended Audience :: Developers',
            'Topic :: Software Development :: Libraries',
            'Programming Language :: Python :: 3 :: Only',
            'Programming Language :: Python :: 3.6',
            'Programming Language :: Python :: 3.7',
        ],
        keywords='topdesk supporting files rest api client',
    )


if __name__ == '__main__':
    run_setup()