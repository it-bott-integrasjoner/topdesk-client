"""
Models used by the client
"""
import datetime
import json
from enum import Enum
from typing import Optional, List

import pydantic
from pydantic import constr
from pydantic.fields import Field


class BaseModel(pydantic.BaseModel):
    @classmethod
    def from_dict(cls, data):
        return cls(**data)

    @classmethod
    def from_json(cls, json_data):
        data = json.loads(json_data)
        return cls.from_dict(data)

    def dict(self, by_alias=True, exclude_unset=True, *args, **kwargs):
        return super().dict(
            by_alias=by_alias, exclude_unset=exclude_unset, *args, **kwargs)

    def json(self, by_alias=True, exclude_unset=True, *args, **kwargs):
        return super().json(
            by_alias=by_alias, exclude_unset=exclude_unset, *args, **kwargs)


class Identifier(BaseModel):
    """ An object with only an ID that references some entity. """
    id: str


class Reference(BaseModel):
    """ Also called a searchlist. """
    id: str
    name: str


class OptionalFields(BaseModel):
    """ An abomination. """
    boolean1: Optional[bool]
    boolean2: Optional[bool]
    boolean3: Optional[bool]
    boolean4: Optional[bool]
    boolean5: Optional[bool]
    number1: Optional[int]
    number2: Optional[int]
    number3: Optional[int]
    number4: Optional[int]
    number5: Optional[int]
    date1: Optional[datetime.datetime]
    date2: Optional[datetime.datetime]
    date3: Optional[datetime.datetime]
    date4: Optional[datetime.datetime]
    date5: Optional[datetime.datetime]
    text1: Optional[str]
    text2: Optional[str]
    text3: Optional[str]
    text4: Optional[str]
    text5: Optional[str]
    searchlist1: Optional[Reference]
    searchlist2: Optional[Reference]
    searchlist3: Optional[Reference]
    searchlist4: Optional[Reference]
    searchlist5: Optional[Reference]


class AddressTypeEnum(str, Enum):
    gb = 'GB'
    nl = 'NL'
    memo = 'MEMO'


class Address(BaseModel):
    street: Optional[str] = ''
    number: Optional[str] = ''
    country: Optional[Reference]
    city: Optional[str] = ''
    county: Optional[str] = ''
    postcode: Optional[str] = ''
    memo: Optional[str] = ''
    type: Optional[AddressTypeEnum]

    class Config:
        fields = {
            'memo': {'alias': 'addressMemo'},
            'type': {'alias': 'addressType'},
        }


class BranchTypeEnum(str, Enum):
    independent = 'independentBranch'
    head = 'headBranch'
    has_a_head = 'hasAHeadBranch'


class Branch(BaseModel):
    id: str = None
    name: str = ''
    specification: str = ''
    client_reference_number: str = ''
    timezone: str = None
    extra_a: Optional[Reference]
    extra_b: Optional[Reference]
    phone: str = ''
    fax: str = ''
    address: Address = None
    postal_address: Address = None
    email: str = ''
    website: str = ''
    type: Optional[BranchTypeEnum]
    head_branch: Optional[Identifier]
    creator: Optional[Reference]
    created: Optional[datetime.datetime]
    modified: Optional[datetime.datetime]
    modifier: Optional[Reference]
    optional_fields_1: Optional[OptionalFields]
    optional_fields_2: Optional[OptionalFields]

    class Config:
        fields = {
            'client_reference_number': {'alias': 'clientReferenceNumber'},
            'timezone': {'alias': 'timeZone'},
            'extra_a': {'alias': 'extraA'},
            'extra_b': {'alias': 'extraB'},
            'postal_address': {'alias': 'postalAddress'},
            'type': {'alias': 'branchType'},
            'head_branch': {'alias': 'headBranch'},
            'optional_fields_1': {'alias': 'optionalFields1'},
            'optional_fields_2': {'alias': 'optionalFields2'},
            'created': {'alias': 'creationDate'},
            'modified': {'alias': 'modificationDate'},
        }

    def dict(self, *args, **kwargs):
        kwargs['exclude'] = {'id'}
        return super().dict(*args, **kwargs)


class BranchReference(BaseModel):
    id: str
    name: str
    client_reference_number: Optional[str]
    timezone: Optional[str]
    extra_a: Optional[Reference]
    extra_b: Optional[Reference]

    class Config:
        fields = {
            'client_reference_number': {'alias': 'clientReferenceNumber'},
            'timezone': {'alias': 'timeZone'},
            'extra_a': {'alias': 'extraA'},
            'extra_b': {'alias': 'extraB'},
        }

    def dict(self, *args, **kwargs):
        kwargs['include'] = {'id'}
        return super(BranchReference, self).dict(*args, **kwargs)

    def inclusive_dict(self, *args, **kwargs):
        return super(BranchReference, self).dict(*args, **kwargs)


class Operator(BaseModel):
    id: Optional[str]
    first_name: str
    last_name: str
    user_name: str
    phone: Optional[str]
    mobile: Optional[str]
    email: str
    branch: BranchReference
    # TODO: Remove the password field when proper auth is in place
    password: Optional[str]
    installer: Optional[bool] = False
    loginPermission: Optional[bool] = False
    firstLineCallOperator: Optional[bool] = False
    secondLineCallOperator: Optional[bool] = False
    problemManager: Optional[bool] = False
    problemOperator: Optional[bool] = False
    knowledgeBaseManager: Optional[bool] = False

    class Config:
        fields = {
            'first_name': {'alias': 'firstName'},
            'last_name': {'alias': 'surName'},
            'user_name': {'alias': 'loginName'},
            'phone': {'alias': 'telephone'},
            'mobile': {'alias': 'mobileNumber'},
        }

    def dict(self, *args, **kwargs):
        kwargs['exclude'] = {'id'}
        kwargs['exclude_unset'] = False
        return super(Operator, self).dict(*args, **kwargs)

    def inclusive_dict(self, *args, **kwargs):
        kwargs['exclude_unset'] = False
        return super(Operator, self).dict(*args, **kwargs)


class OperatorGroup(BaseModel):
    id: str
    groupName: str
    # TODO: add the other 40 fields if needed

    class Config:
        fields = {
            'name': {'alias': 'groupName'},
        }


class PermissionGroup(BaseModel):
    id: str
    name: str


class ExternalLink(BaseModel):
    id: str
    type: str
    date: str = datetime.datetime.now().strftime('%Y-%m-%dT%H:%M:%SZ')


class Department(BaseModel):
    id: Optional[str]
    name: str
    externalLinks: List[ExternalLink] = []

    def dict(self, *args, **kwargs):
        kwargs['exclude'] = {'id'}
        return super().dict(*args, **kwargs)


class DepartmentRef(BaseModel):
    id: str
    name: str

    def dict(self, *args, **kwargs):
        kwargs['include'] = {'id'}
        return super().dict(*args, **kwargs)


class BudgetHolder(BaseModel):
    id: Optional[str]
    name: str
    externalLink: List[ExternalLink] = []

    def dict(self, *args, **kwargs):
        kwargs['exclude'] = {'id'}
        return super().dict(*args, **kwargs)


class BudgetHolderRef(BaseModel):
    id: str
    name: str

    def dict(self, *args, **kwargs):
        kwargs['include'] = {'id'}
        return super().dict(*args, **kwargs)


class ExtraA(BaseModel):
    id: Optional[str]
    name: str
    externalLink: List[ExternalLink] = []

    def dict(self, *args, **kwargs):
        kwargs['exclude'] = {'id'}
        return super().dict(*args, **kwargs)


class ExtraARef(BaseModel):
    id: str
    name: Optional[str]

    def dict(self, *args, **kwargs):
        kwargs['include'] = {'id'}
        return super().dict(*args, **kwargs)


class Person(BaseModel):
    id: Optional[str]
    first_name: str
    last_name: str
    user_name: str
    networkLoginName: str
    phone: Optional[str]
    mobile: Optional[str]
    email: str
    employee_number: Optional[str]
    branch: BranchReference
    # TODO: Remove the password field when proper auth is in place
    password: Optional[str]

    department: Optional[DepartmentRef]
    budgetHolder: Optional[BudgetHolderRef]
    jobTitle: Optional[str]
    personExtraFieldA: Optional[ExtraARef]

    class Config:
        fields = {
            'first_name': 'firstName',
            'last_name': 'surName',
            'user_name': 'tasLoginName',
            'employee_number': 'employeeNumber',
            'phone': 'phoneNumber',
            'mobile': 'mobileNumber'
        }

    def dict(self, *args, **kwargs):
        kwargs['exclude'] = {'id'}
        return super(Person, self).dict(*args, **kwargs)

    def inclusive_dict(self, *args, **kwargs):
        return super(Person, self).dict(*args, **kwargs)


class Status(Enum):
    firstLine = 'firstLine'
    secondLine = 'secondLine'
    partial = 'partial'
    firstLineArchived = 'firstLineArchived'
    secondLineArchived = 'secondLineArchived'
    partialArchived = 'partialArchived'


class IncidentBranch(BaseModel):
    clientReferenceNumber: Optional[str] = None
    timeZone: Optional[str] = None
    extraA: Optional[Reference] = None
    extraB: Optional[Reference] = None
    id: Optional[str] = None
    name: Optional[str] = None


class Caller(BaseModel):
    id: Optional[str] = None
    dynamicName: Optional[str] = None
    email: Optional[str] = None
    mobileNumber: Optional[str] = None
    phoneNumber: Optional[str] = None
    branch: Optional[IncidentBranch] = None


class Object(BaseModel):
    id: Optional[str] = None
    name: Optional[str] = None
    type: Optional[Reference] = None
    make: Optional[Reference] = None
    model: Optional[Reference] = None
    branch: Optional[Reference] = None
    location: Optional[Reference] = None
    specification: Optional[str] = None
    serialNumber: Optional[str] = None


class Location(BaseModel):
    id: Optional[str] = None
    branch: Optional[Branch] = None
    name: Optional[str] = None
    room: Optional[str] = None


class Sla(BaseModel):
    id: Optional[str] = None
    responseTargetDate: Optional[datetime.datetime] = None
    targetDate: Optional[datetime.datetime] = None


class Supplier(BaseModel):
    id: Optional[str] = None
    name: Optional[str] = None
    forFirstLine: Optional[bool] = None
    forSecondLine: Optional[bool] = None


class EscalationStatus(Enum):
    Escalated = 'Escalated'
    Deescalated = 'Deescalated'


class MajorCallObject(BaseModel):
    name: Optional[str] = None
    id: Optional[str] = None
    status: Optional[float] = None
    majorIncident: Optional[bool] = None


class MainIncident(BaseModel):
    id: Optional[str] = None
    number: Optional[str] = None


class PartialIncident(BaseModel):
    link: Optional[str] = None


class Status2(Enum):
    firstLine = 'firstLine'
    secondLine = 'secondLine'
    partial = 'partial'


class Incident(BaseModel):
    id: Optional[str] = Field(None)
    status: Optional[Status2] = None
    number: Optional[str] = Field(None)
    request: Optional[str] = Field(None)
    requests: Optional[str] = Field(None)
    action: Optional[str] = Field(None)
    attachments: Optional[str] = Field(None)
    caller: Optional[Caller] = None
    callerBranch: Optional[IncidentBranch] = Field(None)
    branchExtraFieldA: Optional[Reference] = Field(None)
    branchExtraFieldB: Optional[Reference] = Field(None)
    departmentId: Optional[str] = Field(None)
    budgetHolderId: Optional[str] = Field(None)
    briefDescription: Optional[str] = None
    category: Optional[Reference] = None
    subcategory: Optional[Reference] = None
    callType: Optional[Reference] = None
    entryType: Optional[Reference] = None
    object: Optional[Object] = None
    asset: Optional[Identifier] = None
    branch: Optional[IncidentBranch] = None
    location: Optional[Location] = None
    impact: Optional[Reference] = None
    urgency: Optional[Reference] = None
    priority: Optional[Reference] = None
    duration: Optional[Reference] = None
    actualDuration: Optional[float] = Field(None)
    targetDate: Optional[datetime.datetime] = None
    sla: Optional[Sla] = None
    responseDate: Optional[datetime.datetime] = None
    onHold: Optional[bool] = None
    onHoldDate: Optional[datetime.datetime] = None
    onHoldDuration: Optional[float] = None
    feedbackMessage: Optional[str] = None
    feedbackRating: Optional[float] = None
    operator: Optional[Identifier] = None
    operatorGroup: Optional[Reference] = None
    supplier: Optional[Supplier] = None
    processingStatus: Optional[Reference] = None
    completed: Optional[bool] = None
    completedDate: Optional[datetime.datetime] = None
    closed: Optional[bool] = None
    closedDate: Optional[datetime.datetime] = None
    closureCode: Optional[Reference] = None
    timeSpent: Optional[float] = None
    timeSpentFirstLine: Optional[float] = None
    timeSpentSecondLineAndPartials: Optional[float] = None
    itemCosts: Optional[float] = None
    objectCosts: Optional[float] = None
    costs: Optional[float] = None
    escalationStatus: Optional[EscalationStatus] = None
    escalationReason: Optional[Reference] = None
    escalationOperator: Optional[Reference] = None
    callDate: Optional[datetime.datetime] = None
    creator: Optional[Reference] = None
    creationDate: Optional[datetime.datetime] = None
    modifier: Optional[Reference] = None
    modificationDate: Optional[datetime.datetime] = None
    archivingReason: Optional[Reference] = None
    majorCall: Optional[bool] = None
    majorCallObject: Optional[MajorCallObject] = None
    publishToSsd: Optional[bool] = None
    monitored: Optional[bool] = None
    responded: Optional[bool] = None
    expectedTimeSpent: Optional[float] = None
    mainIncident: Optional[MainIncident] = None
    partialIncidents: Optional[List[PartialIncident]] = None
    optionalFields1: Optional[OptionalFields] = None
    optionalFields2: Optional[OptionalFields] = None
    externalLinks: Optional[List[ExternalLink]] = None


class CallerCreate(BaseModel):
    branch: Optional[Identifier] = Field(None)
    dynamicName: Optional[constr(max_length=109)] = Field(None)
    phoneNumber: Optional[constr(max_length=25)] = Field(None)
    mobileNumber: Optional[constr(max_length=25)] = Field(None)
    email: Optional[constr(max_length=100)] = Field(None)
    department: Optional[Department] = Field(None)
    locations: Optional[Identifier] = Field(None)
    budgetHolder: Optional[BudgetHolder] = Field(None)
    personExtraFieldA: Optional[Reference] = Field(None)
    personExtraFieldB: Optional[Reference] = Field(None)


class TargetDate(BaseModel):
    id: Optional[datetime.datetime] = None


class FeedbackRating(Enum):
    number_1 = 1
    number_2 = 2
    number_3 = 3
    number_4 = 4
    number_5 = 5


class IncidentCreateBody(BaseModel):
    caller: Optional[CallerCreate] = None
    callerLookup: Optional[Identifier] = Field(None)
    status: Optional[Status2] = Field('firstLine')
    briefDescription: Optional[constr(max_length=80)] = Field(None)
    request: Optional[str] = Field(None)
    action: Optional[str] = Field(None)
    actionInvisibleForCaller: Optional[bool] = Field(False)
    entryType: Optional[Reference] = Field(None)
    callType: Optional[Reference] = Field(None)
    category: Optional[Reference] = Field(None)
    subcategory: Optional[Reference] = Field(None)
    externalNumber: Optional[constr(max_length=60)] = Field(None)
    object: Optional[Reference] = Field(None)
    location: Optional[Identifier] = Field(None)
    branch: Optional[Identifier] = Field(None)
    mainIncident: Optional[MainIncident] = Field(None)
    impact: Optional[Reference] = Field(None)
    urgency: Optional[Reference] = Field(None)
    priority: Optional[Reference] = Field(None)
    duration: Optional[Reference] = Field(None)
    targetDate: Optional[TargetDate] = Field(None)
    sla: Optional[Identifier] = Field(None)
    onHold: Optional[bool] = Field(None)
    operator: Optional[Identifier] = Field(None)
    operatorGroup: Optional[Identifier] = Field(None)
    supplier: Optional[Identifier] = Field(None)
    processingStatus: Optional[Reference] = Field(None)
    responded: Optional[bool] = Field(None)
    responseDate: Optional[datetime.datetime] = Field(None)
    completed: Optional[bool] = Field(None)
    completedDate: Optional[datetime.datetime] = Field(None)
    closed: Optional[bool] = Field(None)
    closedDate: Optional[datetime.datetime] = Field(None)
    closureCode: Optional[Reference] = Field(None)
    costs: Optional[float] = Field(None)
    feedbackRating: Optional[FeedbackRating] = Field(None)
    feedbackMessage: Optional[str] = Field(None)
    majorCall: Optional[bool] = Field(None)
    majorCallObject: Optional[Reference] = Field(None)
    publishToSsd: Optional[bool] = Field(None)
    optionalFields1: Optional[OptionalFields] = None
    optionalFields2: Optional[OptionalFields] = None
    externalLink: Optional[ExternalLink] = Field(None)
