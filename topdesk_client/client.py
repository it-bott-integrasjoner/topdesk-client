import logging
import requests
import uuid
import json

from typing import Optional, List, Iterator, Union
from urllib.parse import urljoin, urlparse

from .models import (Branch, BranchReference, Incident, Operator, OperatorGroup,
                     PermissionGroup,
                     Person, DepartmentRef, BudgetHolderRef,
                     ExtraA, ExtraARef, IncidentCreateBody)

logger = logging.getLogger(__name__)


class Endpoints:
    def __init__(self, base_url):
        self.base_url = base_url

    def _prepend_base_url(self, path):
        return urljoin(self.base_url, path)

    def list_branches(self):
        return self._prepend_base_url('branches/')

    def list_operatorgroups(self):
        return self._prepend_base_url('operatorgroups')

    def list_permissiongroups(self):
        return self._prepend_base_url('permissiongroups')

    def get_branch(self, branch_id):
        return urljoin(self._prepend_base_url('branches/id/'), branch_id)

    def get_departments(self):
        return self._prepend_base_url('departments')

    def get_budget_holder(self):
        return self._prepend_base_url('budgetholders')

    def get_operator(self, identity):
        return urljoin(self._prepend_base_url('operators/id/'), identity)

    def get_operators(self):
        return self._prepend_base_url('operators/')

    def get_operator_filters(self):
        return self._prepend_base_url('operators/filters/operator')

    def get_operator_filter(self, identity):
        return urljoin(self.get_operator(identity) + '/', 'filters/operator')

    def get_person(self, identity):
        return urljoin(self._prepend_base_url('persons/id/'), identity)

    def get_persons(self):
        return self._prepend_base_url('persons/')

    def get_operators_operatorgroup(self, operator_id):
        return urljoin(
            urljoin(self._prepend_base_url('operators/id/'),
                    operator_id + '/'),
            'operatorgroups')

    def get_operators_permissiongroup(self, operator_id):
        return urljoin(
            urljoin(self._prepend_base_url('operators/id/'),
                    operator_id + '/'),
            'permissiongroups')

    def get_extra_fields_a(self):
        return self._prepend_base_url('personExtraFieldAEntries/')

    def get_incidents(self):
        return self._prepend_base_url("incidents")

    def get_incident(self, incident_id: str):
        return urljoin(self._prepend_base_url("incidents/id/"), incident_id)


class TopDeskClient:
    def __init__(self,
                 url,
                 username=None,
                 password=None,
                 headers={},
                 rewrite_url=None,
                 use_sessions=True):
        self.urls = Endpoints(url)
        self.rewrite_url = rewrite_url
        self.headers = headers
        if username and password:
            self.auth = requests.auth.HTTPBasicAuth(username, password)
        else:
            self.auth = None

        if use_sessions:
            self.session = requests.Session()
        else:
            self.session = requests

    def _build_headers(self, headers):
        request_headers = {}
        for h in (self.headers):
            request_headers[h] = self.headers[h]
        for h in (headers or ()):
            request_headers[h] = headers[h]
        return request_headers

    def call(self,
             method_name,
             url,
             headers=None,
             params=None,
             return_response=False,
             **kwargs):
        logger.debug('Calling %s %s with params=%r',
                     method_name,
                     urlparse(url).path,
                     params)
        r = self.session.request(method_name,
                                 (url if self.rewrite_url is None
                                  else url.replace(*self.rewrite_url)),
                                 auth=self.auth,
                                 headers=(self._build_headers({}) if headers is None else
                                          self._build_headers(headers)),
                                 params=params if params is not None else {},
                                 **kwargs)
        logger.debug('GOT HTTP %d: %r', r.status_code, r.content)

        if return_response:
            return r
        else:
            r.raise_for_status()
            return r.json()

    def post(self, url, **kwargs):
        return self.call('POST', url, **kwargs)

    def get(self, url, **kwargs):
        return self.call('GET', url, **kwargs)

    def put(self, url, **kwargs):
        return self.call('PUT', url, **kwargs)

    def patch(self, url, **kwargs):
        return self.call('PATCH', url, **kwargs)

    def delete(self, url, **kwargs):
        return self.call('DELETE', url, **kwargs)

    def depaginate(self, url, page_size=100):
        offset = 0
        while True:
            params = {
                'page_size': page_size,
                'start': offset,
            }
            page = self.get(url, params=params, return_response=True)
            if page.status_code == 204:
                break
            data = page.json()
            for item in data:
                yield item
            num = len(data)
            if num < page_size:
                break
            if num == page_size:
                offset += num

    def include_fields(self, params: dict, fields: Optional[List[str]] = None) -> None:
        if not fields:
            return
        params['$fields'] = ','.join(fields)

    def create_department(self, department):
        url = self.urls.get_departments()
        r = self.post(url,
                      return_response=True,
                      headers={'Content-Type': 'application/json'},
                      data=department.json())
        if r.status_code == 201:
            return DepartmentRef.from_json(r.content)
        else:
            r.raise_for_status()
            return None

    def get_department(self, department):
        url = self.urls.get_departments()
        r = self.get(url, return_response=True)
        if r.status_code == 200:
            for x in r.json():
                if x.get('name') == department:
                    return DepartmentRef.from_dict(x)
            return None
        elif r.status_code in (404, 500):
            return None
        else:
            r.raise_for_status()
            return None

    def create_budget_holder(self, budget_holder):
        url = self.urls.get_budget_holder()
        r = self.post(url,
                      return_response=True,
                      headers={'Content-Type': 'application/json'},
                      data=budget_holder.json())
        if r.status_code == 201:
            return BudgetHolderRef.from_json(r.content)
        else:
            r.raise_for_status()
            return None

    def get_budget_holder(self, budget_holder):
        url = self.urls.get_budget_holder()
        r = self.get(url, return_response=True)
        if r.status_code == 200:
            for x in r.json():
                if x.get('name') == budget_holder:
                    return BudgetHolderRef.from_dict(x)
            return None
        elif r.status_code == 404:
            return None
        else:
            r.raise_for_status()
            return None

    def get_operator(self, identity):
        def _get_operator_by_id(ident):
            url = self.urls.get_operator(ident)
            r = self.get(url, return_response=True)
            if r.status_code == 200:
                return Operator.from_dict(r.json())
            else:
                r.raise_for_status()
                return None

        def _get_operator_by_username(name):
            url = self.urls.get_operators()
            r = self.get(url,
                         params={'topdesk_login_name': name},
                         return_response=True)
            if r.status_code == 204:
                return None
            else:
                r.raise_for_status()

            f = filter(lambda x: x.user_name.lower() == name,
                       [Operator.from_dict(x) for x in r.json()])
            try:
                return next(f)
            except StopIteration:
                return None

        if isinstance(identity, Operator) and identity.id:
            return _get_operator_by_id(identity.id)
        elif isinstance(identity, Operator):
            return _get_operator_by_username(identity.user_name)
        else:
            return _get_operator_by_username(identity)

    def create_operator(self, operator):
        url = self.urls.get_operators()
        # TODO: Remove password setting when proper auth arrives
        op = operator.copy()
        op.password = uuid.uuid4()
        r = self.post(url,
                      return_response=True,
                      data=op.json())
        if r.status_code == 201:
            return Operator.from_dict(r.json())
        else:
            return None

    def update_operator(self, operator):
        url = self.urls.get_operator(operator.id)
        # TODO: Remove password setting when proper auth arrives
        op = operator.copy()
        op.password = uuid.uuid4()
        r = self.put(url,
                     return_response=True,
                     data=op.json())
        if r.status_code == 200:
            return Operator.from_dict(r.json())
        else:
            return None

    def link_operator_to_operator_filter(self, operator, filter_id):
        url = self.urls.get_operator_filter(operator.id)
        r = self.post(url,
                      headers={'Content-Type': 'application/json'},
                      return_response=True,
                      data=json.dumps([{'id': filter_id}]))
        if r.status_code == 204:
            return True
        else:
            return [x.get('message') for x in r.json()]

    def list_operator_filters(self):
        return self.get(self.urls.get_operator_filters(),
                        return_response=True).json()

    def list_operators(self):
        for x in self.get(self.urls.get_operators()):
            yield Operator.from_dict(x)

    def list_operatorgroups(self):
        url = self.urls.list_operatorgroups()
        for x in self.depaginate(url):
            yield OperatorGroup.from_dict(x)

    def list_permissiongroups(self):
        url = self.urls.list_permissiongroups()
        for x in self.depaginate(url):
            yield PermissionGroup.from_dict(x)

    def get_person(self, identity):
        if isinstance(identity, Person):
            url = self.urls.get_person(identity.id)
            r = self.get(url, return_response=True)
            if r.status_code == 200:
                return Person.from_dict(r.json())
            else:
                return None
        else:
            url = self.urls.get_persons()
            r = self.get(url,
                         params={'ssp_login_name': identity},
                         return_response=True)

            if r.status_code == 200:
                persons = [Person.from_dict(x) for x in r.json()]
                f = filter(lambda x: x.user_name.lower() == identity,
                           persons)
                try:
                    return next(f)
                except StopIteration:
                    return None
            else:
                r.raise_for_status()
                return None

    def create_person(self, person):
        url = self.urls.get_persons()
        # TODO: Remove password setting when proper auth arrives
        pe = person.copy()
        pe.password = uuid.uuid4()
        return self.post(url,
                         return_response=True,
                         data=pe.json())

    def update_person(self, person):
        url = self.urls.get_person(person.id)
        # TODO: Remove password setting when proper auth arrives
        pe = person.copy()
        pe.password = uuid.uuid4()
        return self.patch(url,
                          return_response=True,
                          data=pe.json())

    def list_persons(self):
        for x in self.get(self.urls.get_persons()):
            yield Person.from_dict(x)

    def list_branches(self, fields: Optional[list] = None, name_filter: Optional[str] = None) -> Iterator[BranchReference]:
        params = {}
        self.include_fields(params,
                            fields or ['id', 'name', 'clientReferenceNumber'])
        if name_filter:
            params['nameFragment'] = name_filter
        for x in self.get(self.urls.list_branches(), params=params):
            yield BranchReference.from_dict(x)

    def get_branch(self, branch_id: str) -> Optional[Branch]:
        url = self.urls.get_branch(branch_id)
        r = self.get(url, return_response=True)
        if r.status_code == 200:
            return Branch.from_dict(r.json())
        return None

    def create_branch(self, branch: Branch):
        url = self.urls.list_branches()
        return self.post(url,
                         return_response=True,
                         data=branch.json())

    def patch_branch(self, branch_id: str, data: dict) -> Optional[Branch]:
        url = self.urls.get_branch(branch_id)
        r = self.patch(url,
                       return_response=True,
                       json=data)
        r.raise_for_status()
        if r.status_code == 200:
            return Branch.from_dict(r.json())
        return None

    def _do_group_op(self, url_generator, operator_id, group_id, operation):
        url = url_generator(operator_id)
        r = operation(url,
                      return_response=True,
                      json=[{'id': group_id}])

        if r.status_code == 204:
            return None
        else:
            r.raise_for_status()
            return r

    @staticmethod
    def _select_group_id_by_type(group: Union[OperatorGroup, PermissionGroup, str]):
        if isinstance(group, str):
            return group
        else:
            return group.id

    def add_operator_to_operator_group(self, operator: Operator, operator_group: Union[OperatorGroup, str]):
        return self._do_group_op(self.urls.get_operators_operatorgroup,
                                 operator.id,
                                 self._select_group_id_by_type(operator_group),
                                 self.post)

    def add_operator_to_permission_group(self, operator: Operator, permission_group: Union[PermissionGroup, str]):
        return self._do_group_op(self.urls.get_operators_permissiongroup,
                                 operator.id,
                                 self._select_group_id_by_type(permission_group),
                                 self.post)

    def remove_operator_from_operator_group(self, operator: Operator, operator_group: Union[OperatorGroup, str]):
        return self._do_group_op(self.urls.get_operators_operatorgroup,
                                 operator.id,
                                 self._select_group_id_by_type(operator_group),
                                 self.delete)

    def remove_operator_from_permission_group(self, operator: Operator, permission_group: Union[PermissionGroup, str]):
        return self._do_group_op(self.urls.get_operators_permissiongroup,
                                 operator.id,
                                 self._select_group_id_by_type(permission_group),
                                 self.delete)

    def list_extra_fields_a(self) -> List[ExtraARef]:
        r = self.get(self.urls.get_extra_fields_a(), return_response=True)
        if r.status_code == 200:
            return [ExtraARef.from_dict(x) for x in r.json()]
        else:
            r.raise_for_status()
            return None

    def create_extra_fields_a(self, extra_a: ExtraA) -> List[ExtraA]:
        r = self.post(self.urls.get_extra_fields_a(),
                      headers={'Content-Type': 'application/json'},
                      return_response=True,
                      data=extra_a.json())
        if r.status_code == 201:
            return ExtraA.from_dict(r.json())
        else:
            r.raise_for_status()
            return None

    def post_incident(self, incident_cb: IncidentCreateBody) -> Optional[Incident]:
        """POST an incident to Topdesk

        Any fields that are not explicitly set in the model will be
        excluded upon POST to Topdesk. Since most fields are optional
        there is no point to sending an enormous object where almost
        all fields have value null.
        """
        url = self.urls.get_incidents()
        r = self.post(
            url,
            headers={'Content-Type': 'application/json'},
            return_response=True,
            data=incident_cb.json(exclude_unset=True)
        )
        r.raise_for_status()
        if r.status_code == 201:
            return Incident.from_dict(r.json())
        return None

    def get_incidents(self):
        raise NotImplementedError

    def get_incident(self, identifier: str):
        """GET a single incident"""
        url = self.urls.get_incident(identifier)
        r = self.get(url, return_response=True)
        if r.status_code == 200:
            return Incident.from_dict(r.json())
        return None

    def patch_incident(self, identifier: str):
        raise NotImplementedError

    def put_incident(self, identifier: str):
        raise NotImplementedError


def get_client(config):
    return TopDeskClient(**config)
